const { app, BrowserWindow } = require('electron');


let mainWindow;

app.on('ready', () => {
	mainWindow = new BrowserWindow({ width: 1280, height: 720, resizable: false });
	mainWindow.loadURL('file://' + __dirname + '/public/index.html');
})
