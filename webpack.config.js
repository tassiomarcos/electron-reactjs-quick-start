'use strict'
const path = require('path');
const fs = require('fs');
const webpack = require('webpack');
const CompressionPlugin = require('compression-webpack-plugin');

const nodeModules = {}
fs.readdirSync('node_modules')
  .filter(x => {
    return ['.bin'].indexOf(x) === -1
  })
  .forEach(mod => {
    nodeModules[mod] = `commonjs ${mod}`
  })

module.exports = {

    entry: './src/index.js',
    output: {
        path: path.join(__dirname + '/public'),
        filename: 'bundle.js'
    },
    devServer: {
        inline: true,
        contentBase: './public',
        port: 3333
    },
    module:{

        loaders:[{
            test: /\.js$/,
            exclude: /node_modules/,
            loader: 'babel-loader',
            query:{
                presets: ['es2017', 'env', "stage-2"]
            }},
            {
                test: /\.css$/,
                loader: [ 'style-loader', 'css-loader' ]
            },]
    },
    node: {
        fs: 'empty',
    },
    plugins : [
        new webpack.DefinePlugin({
            'process.ev': {
                'NODE_ENV': JSON.stringify('production')
            }
        }),
    new webpack.optimize.UglifyJsPlugin({
      mangle: true,
      compress: {
        warnings: false, // Suppress uglification warnings
        pure_getters: true,
        unsafe: true,
        unsafe_comps: true,
        screw_ie8: true
      },
      output: {
        comments: false,
      },
      exclude: [/\.min\.js$/gi] // skip pre-minified libs
    }),
    new webpack.IgnorePlugin(/^\.\/locale$/, [/moment$/]),
    new webpack.NoEmitOnErrorsPlugin(),
  /*  new CompressionPlugin({
      asset: "[path].gz[query]",
      algorithm: "gzip",
      test: /\.js$|\.css$|\.html$/,
      threshold: 10240,
      minRatio: 0
    }), */
        new webpack.optimize.AggressiveMergingPlugin(),
    ],
    externals: [ nodeModules ]

}